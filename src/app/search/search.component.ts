import {Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output() setFilterOptions = new EventEmitter();
  types: string[] = ['Grass', 'Poison', 'Fire', 'Flying', 'Water', 'Bug', 'Normal', 'Fighting', 'Rock', 'Steel', 'Ice', 'Ghost', 'Dragon', 'Psychic', 'Ground', 'Electric'];
  name: string;
  id: number;
  selectedType: string;
  constructor() { }

  ngOnInit(): void {
  }

  selectType(type): void{
    this.selectedType = type;
    this.sendOptions();
  }

  sendOptions(): void {
    this.setFilterOptions.emit({
      name: this.name,
      id: this.id,
      type: this.selectedType
    });
  }
}
