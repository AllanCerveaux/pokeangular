import { Pipe, PipeTransform } from '@angular/core';
import { Filter, PokemonService } from '@core/*';

@Pipe({
  name: 'pokemonFilter'
})
export class PokemonFilterPipe implements PipeTransform {
  constructor(private pokemonService: PokemonService) {}

  transform(items, { name, id, type }: Filter): object[] {
    if (!items) { return []; }
    if (!name && !id && !type) { return items; }
    if (name) {
      items = this.pokemonService.findByName(items, name);
    }
    if (id) {
      items = this.pokemonService.findById(items, id);
    }
    if (type) {
      items = this.pokemonService.findByType(items, type);
    }
    return items;
  }

}
