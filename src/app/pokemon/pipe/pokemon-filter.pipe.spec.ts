import { PokemonFilterPipe } from './pokemon-filter.pipe';
import { PokemonService } from '@core/*';

describe('PokemonFilterPipe', () => {
  const pokemonService = new PokemonService();
  const pipe = new PokemonFilterPipe(pokemonService);

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
