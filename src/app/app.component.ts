import {Component} from '@angular/core';
// @ts-ignore
import Pokemon from './data/seed.json';

import { Filter } from '@core/*';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pokangular';
  pokemonList: Filter[] = Pokemon;
  filterOptions: Filter = {
    name: '',
    id: null,
    type: ''
  };

  setFilter($e): void {
    this.filterOptions = {
      name: $e.name,
      id: $e.id,
      type: $e.type
    };
  }
}
