import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { PokemonModule } from './pokemon/pokemon.module';
import { CoreModule } from '@core/*';
import { SearchModule } from './search/search.module';

import { PokemonFilterPipe } from './pokemon/pipe/pokemon-filter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    PokemonFilterPipe
  ],
  imports: [
    BrowserModule,
    PokemonModule,
    CoreModule,
    SearchModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
