import { Injectable } from '@angular/core';
import {Filter} from '../../models';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor() { }

  findByName(arr: Filter[], term: string): object[]{
    return arr.filter(({ name }) => name.toLowerCase().includes(term.trim()));
  }

  findById(arr: Filter[], term: number): object[]{
    return arr.filter(({ id }) => parseInt(String(id), 10) === term)
  }

  findByType(arr: Filter[], term: string): object[]{
    return  arr.filter(({ type }) => type.includes(term));
  }
}
