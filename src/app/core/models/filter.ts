export interface Filter {
  name: string;
  id: number;
  type: string;
}
