import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokemonService } from './services';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    PokemonService
  ]
})
export class CoreModule { }
